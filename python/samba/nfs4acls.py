# NFS4 ACL compatible SDDL consolidation.
# Copyright (C) Thomas Geppert <geppi@digitx.de> 2021
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

def nfs4_consolidate(sddl, is_file=False):
    """
    Consolidate the SDDL to match the NFS4 ACL
    as processed by nfs4_acls.c: nfs4_acl_add_sec_ace() and check_for_duplicate_sec_ace().

    For files all inheritance ACE flags are removed and ACEs with Creator/Owner trustee are removed.
    Duplicate ACEs are removed.
    """
    acl = sddl.split('(')
    consolidated_acl = [acl.pop(0)]
    for ace in acl:
        ace_fields = ace.split(';')
        if is_file:
            if ace_fields[-1] == 'CO)' or ace_fields[-1] == 'S-1-3-0)':
                continue
            flags = ace_fields[1].replace('OI','').replace('CI','').replace('NP','').replace('IO','')
        else:
            flags = ace_fields[1]
        ace = '({0};{1};{2};{3};{4};{5}'.format(ace_fields[0], flags, ace_fields[2], ace_fields[3], ace_fields[4], ace_fields[5])
        if ace not in consolidated_acl:
            consolidated_acl.append(ace)

    return ''.join(consolidated_acl)
